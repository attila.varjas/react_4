import React from 'react';

export default function NotFound() {
    return (
        <div>
            <h1>Not Found</h1>
            <p className="lead">The page that you are looking for does not exists..</p>
        </div>
    );
}