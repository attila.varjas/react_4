import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function Navbar(props) {
    return (
        <nav className="navbar bg-primary">
            <div>
                <h1>{props.title}</h1>
            </div>
            <ul>
                <li>
                    <Link to='/'>Home</Link>
                </li>
                <li>
                    <Link to='/about'>About</Link>
                </li>
            </ul>
        </nav>
    )
}

Navbar.defaultProps = {
    title: 'Github finder'
}

Navbar.propTypes = {
    title: PropTypes.string.isRequired,
}